

 $(function() {
   $("div.btn").click(function() {

      $(this).toggleClass('red');
      $(this).find('i').toggleClass("fa-angle-down fa-angle-up");
      $(this).next().toggleClass("show hidden");

   });
});

$(function() {
   $(".nav-item .nav-link").click(function() {

      $(".tab-pane").removeClass('active');

   });
});


$('.about-soln .subtitle').mouseenter(function(){
   $(".line").animate({width:"100%", marginTop:"20px"}, 500);
});
$('.about-soln .subtitle').mouseout(function(){
   $(".line").animate({width:"0%", marginTop:"0px"}, 500);
});

var map;
function initMap() {
   map = new google.maps.Map(document.getElementById('mapContainer'), {
      center: {lat: 50.445210, lng: -104.618896},
      zoom: 8
   });
}